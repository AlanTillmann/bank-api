// SERVICE LAYER!
// RELIES ON THE DATA LAYER AND PROVIDES "SERVICES" TO THE API LAYER

import { BankClientDAOPostres } from "../src/daos/bank-client-dao-postgres-impl";
import { BankClient } from "../src/entities";
import BankClientService from "./bank-client-service";

export class BankClientServiceImpl implements BankClientService
{

    bankClientDAO:BankClientDAOPostres = new BankClientDAOPostres();

    registerBankClient(bankClient:BankClient): Promise<BankClient> 
    {
        return this.bankClientDAO.createBankClient(bankClient);
    }

    retrieveAllBankClients(): Promise<BankClient[]> 
    {
        return this.bankClientDAO.getAllBankClients();
    }

    retrieveBankClientById(id:number): Promise<BankClient> 
    {
        return this.bankClientDAO.getBankClientById(id);
    }

    modifyBankClient(bankClient: BankClient): Promise<BankClient> 
    {
        return this.bankClientDAO.updateBankClient(bankClient);
    }

    removeBankClient(id:number): Promise<Boolean>
    {
        return this.bankClientDAO.deleteBankClientById(id);
    }
    
}
//     async checkoutBookById(bookId: number): Promise<Book> 
//     {
//         let book:Book = await this.bookDAO.getBookById(bookId);
//         book.isAvailable = false;
//         book.returnDate = Date.now() + 1_209_600; // you can use underscores to make large nums more readable
//         // we add the seconds for 2 weeks worth of time (1.2mil)
//         book = await this.bookDAO.updateBook(book);
//         return book;
//     }

//     // service methods also perform business logic
//     async checkinBookById(bookId: number): Promise<Book> 
//     {
//         throw new Error("Method not implemented.");
//     }

//     searchByTitle(title: string): Promise<Book[]> 
//     {
//         throw new Error("Method not implemented.");
//     }

//     removeBookById(bookId: number): Promise<boolean> 
//     {
//         throw new Error("Method not implemented.");
//     }
