import { BankClient } from "../src/entities";

export default interface BankClientService
{
    registerBankClient(bankClient:BankClient):Promise<BankClient>;
    retrieveAllBankClients():Promise<BankClient[]>;
    retrieveBankClientById(id:number):Promise<BankClient>;
    modifyBankClient(bankClient:BankClient):Promise<BankClient>; 
    removeBankClient(id:number):Promise<Boolean>;
}