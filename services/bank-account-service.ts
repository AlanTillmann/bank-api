import { BankAccount } from "../src/entities";

export default interface BankAccountService
{
    registerBankAccount(bankAccount:BankAccount):Promise<BankAccount>;
    retrieveAllBankAccounts():Promise<BankAccount[]>;
    retrieveBankAccountById(accountId:number):Promise<BankAccount>;
    modifyBankAccount(bankAccount:BankAccount):Promise<BankAccount>; 
    removeBankAccount(accountId:number):Promise<Boolean>;
}