import { BankAccountDAOPostres } from "../src/daos/bank-account-dao-postgres-impl";
import { BankAccount } from "../src/entities";
import BankAccountService from "./bank-account-service";

export class BankAccountServiceImpl implements BankAccountService
{
    bankAccountDAO:BankAccountDAOPostres = new BankAccountDAOPostres();

    registerBankAccount(bankAccount:BankAccount): Promise<BankAccount> 
    {
        return this.bankAccountDAO.createBankAccount(bankAccount);
    }

    retrieveAllBankAccounts(): Promise<BankAccount[]> 
    {
        return this.bankAccountDAO.getAllBankAccounts();
    }

    retrieveBankAccountById(accountId:number): Promise<BankAccount> 
    {
        return this.bankAccountDAO.getBankAccountById(accountId);
    }

    modifyBankAccount(bankAccount: BankAccount): Promise<BankAccount> 
    {
        return this.bankAccountDAO.updateBankAccount(bankAccount);
    }

    removeBankAccount(accountId:number): Promise<Boolean>
    {
        return this.bankAccountDAO.deleteBankAccountById(accountId);
    }
    
}