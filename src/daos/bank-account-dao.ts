// DAO (Data Access Object)
// A class that is responsible for persisting an entity
import { BankAccount } from "../entities";

// A DAO should support the CRUD operations (Create, Read, Update, Delete)
export interface BankAccountDAO
{
    // CREATE
    createBankAccount(bankAccount:BankAccount):Promise<BankAccount>; // pass in the client and return a promise that will eventually be a client

    // READ
    getAllBankAccounts():Promise<BankAccount[]>; // list of all our clients (in JSON)
    getBankAccountById(accountId:number):Promise<BankAccount>;
    
    // UPDATE
    updateBankAccount(bankAccount:BankAccount):Promise<BankAccount>;

    // DELETE
    deleteBankAccountById(accountId:number):Promise<boolean>;

}