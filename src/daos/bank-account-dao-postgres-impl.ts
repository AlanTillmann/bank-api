import { readFile, writeFile } from "fs/promises";
import { BankAccount } from "../entities";
import { client } from "../connection";
import { MissingResourceError } from "../error";
import { BankAccountDAO } from "./bank-account-dao";

export class BankAccountDAOPostres implements BankAccountDAO
{
    async createBankAccount(bankAccount:BankAccount): Promise<BankAccount> 
    {
        const sql:string = "insert into account (client_id, amount) values ($1, $2) returning account_id";
        const values = [bankAccount.clientId, bankAccount.amount];
        const result = await client.query(sql, values);
        bankAccount.accountId = result.rows[0].account_id;
        return bankAccount;
    }

    async getAllBankAccounts(): Promise<BankAccount[]> 
    {
        const sql:string = 'select * from account';
        const result = await client.query(sql);
        const bankAccounts:BankAccount[] = [];
        for(const row of result.rows)
        {
            const bankAccount:BankAccount = new BankAccount
            (
                row.account_id,
                row.client_id,
                row.amount
            );
            bankAccounts.push(bankAccount);
        }
        return bankAccounts;
    }

    async getBankAccountById(accountId: number): Promise<BankAccount> 
    {
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        const row = result.rows[0];
        const bankAccount:BankAccount = new BankAccount
        (
            row.account_id, // when referencing row data, we need to access the EXACT variable as within the DB
            row.client_id,
            row.amount 
        );
        return bankAccount;
    }

    async updateBankAccount(bankAccount:BankAccount): Promise<BankAccount> 
    {
        const sql:string = 'update account set amount=$1 where account_id=$2';
        const values = [bankAccount.amount, bankAccount.accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${bankAccount.accountId} does not exist`);
        }
        return bankAccount;
    }

    async deleteBankAccountById(accountId: number): Promise<boolean> 
    {
        const sql:string = 'delete from account where account_id=$1'; // sql statement to delete client with specific ID
        const values = [accountId];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The account with id ${accountId} does not exist`);
        }
        return true;
    }
}