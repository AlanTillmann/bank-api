import { readFile, writeFile } from "fs/promises";
import { BankClient } from "../entities";
import { BankClientDAO } from "./bank-client-dao";
import { client } from "../connection";
import { MissingResourceError } from "../error";

export class BankClientDAOPostres implements BankClientDAO
{

    async createBankClient(bankClient: BankClient): Promise<BankClient> 
    {
        const sql:string = "insert into client(fname) values ($1) returning id";
        const values = [bankClient.fname];
        const result = await client.query(sql, values);
        bankClient.id = result.rows[0].id;
        return bankClient;
    }

    async getAllBankClients(): Promise<BankClient[]> 
    {
        const sql:string = 'select * from client';
        const result = await client.query(sql);
        const bankClients:BankClient[] = [];
        for(const row of result.rows)
        {
            const bankClient:BankClient = new BankClient
            (
                row.id,
                row.fname
            );
            bankClients.push(bankClient);
        }
        return bankClients;
    }

    async getBankClientById(id: number): Promise<BankClient> 
    {
        const sql:string = 'select * from client where id = $1';
        const values = [id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${id} does not exist`);
        }
        const row = result.rows[0];
        const bankClient:BankClient = new BankClient
        (
            row.id, 
            row.fname, 
        );
        return bankClient;
    }

    async updateBankClient(bankClient:BankClient): Promise<BankClient> 
    {
        const sql:string = 'update client set fname=$1 where id=$2';
        const values = [bankClient.fname, bankClient.id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${bankClient.id} does not exist`);
        }
        return bankClient;
    }

    async deleteBankClientById(id: number): Promise<boolean> 
    {
        const sql:string = 'delete from client where id=$1'; // sql statement to delete client with specific ID
        const values = [id];
        const result = await client.query(sql, values);
        if(result.rowCount === 0)
        {
            throw new MissingResourceError(`The client with id ${id} does not exist`);
        }
        return true;
    }
}