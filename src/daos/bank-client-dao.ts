// DAO (Data Access Object)
// A class that is responsible for persisting an entity
import { BankClient } from "../entities";

// A DAO should support the CRUD operations (Create, Read, Update, Delete)
export interface BankClientDAO
{
    // CREATE
    createBankClient(bankClient:BankClient):Promise<BankClient>; // pass in the client and return a promise that will eventually be a client

    // READ
    getAllBankClients():Promise<BankClient[]>; // list of all our clients (in JSON)
    getBankClientById(id:number):Promise<BankClient>;
    
    // UPDATE
    updateBankClient(bankClient:BankClient):Promise<BankClient>;

    // DELETE
    deleteBankClientById(id:number):Promise<boolean>;

}