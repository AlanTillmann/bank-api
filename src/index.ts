// THIS IS THE API LAYER!
// RELIES ON THE SERVICES LAYER
import express from 'express';
import BankClientService from '../services/bank-client-service';
import { BankClientServiceImpl } from '../services/bank-client-service-impl';
import { BankClient } from './entities';
import { MissingResourceError } from './error';
import BankAccountService from '../services/bank-account-service';
import { BankAccountServiceImpl } from '../services/bank-account-service-impl';
import { BankAccount } from './entities';


const app = express();
app.use(express.json()); // Middleware

const bankClientService:BankClientService = new BankClientServiceImpl();
const bankAccountService:BankAccountService = new BankAccountServiceImpl();

/*
Endpoint: GET all clients
Description: Gets all clients within our current client table, returning the client ID and first name of each client.
*/
app.get("/clients", async (req, res) =>
{
    const bankClient:BankClient[] = await bankClientService.retrieveAllBankClients();
    res.status(200);
    res.send(bankClient);
});

/*
Endpoint: GET clients by ID
Description: Gets the client with matching ID within our current client table, returning the client ID and first name of the client.
             Returns an error if the specified client ID does not exist.
*/
app.get("/clients/:id", async (req, res) =>
{
    try
    {
        const bankClientId = Number(req.params.id);
        const bankClient:BankClient = await bankClientService.retrieveBankClientById(bankClientId);
        res.status(200);
        res.send(bankClient);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

/*
Endpoint: POST a new client
Description: Registers a new client to our client table.  They will be given a default client ID and specified first name.
             Returns an error if more or less than the first name field is provided.
*/
app.post("/clients", async (req, res) =>
{
    if(Object.keys(req.body).length > 1)
        {
            throw new MissingResourceError("message: Cannot add properties other than first name.");
        }
    let bankClient:BankClient = req.body;
    bankClient = await bankClientService.registerBankClient(bankClient);
    res.status(201);
    res.send(bankClient);
});

/*
Endpoint: PUT update client by ID
Description: Updates the client's first name within our current client table. Client ID provided must match URL ID.
             Returns an error if there are more than 2 properties or the URL ID does not match the ID of the body.
*/
app.put("/clients/:id", async (req, res) =>
{
    try
    {
        if(Object.keys(req.body).length > 2)
        {
            throw new MissingResourceError("message: Cannot add properties other than client ID and first name.");
        }
        const id = Number(req.params.id);
        let bankClient:BankClient = req.body; // gives you all the info you need
        if (id === req.body.id)
        {
            bankClient = await bankClientService.modifyBankClient(bankClient);
            res.status(200);
            res.send(bankClient);
        }
        else
        {
            res.status(403);
            res.send({"message": "Error. ID of URL must match body ID."});
        }
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404); 
            res.send(error);
        }
    }
});

/*
Endpoint: DELETE client
Description: Deletes a client from our client table with matching client ID. Returns an error if the client ID 
             does not exist.
*/
app.delete("/clients/:id", async (req, res) =>
{
    try
    {
        const id = Number(req.params.id);
        let bankClientExists:Boolean = req.body;
        bankClientExists = await bankClientService.removeBankClient(id);
        res.status(205);
        res.send(bankClientExists);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

// ACCOUNTS ---------------------------------------------------------------------------------------------------------------------

/*
Endpoint: POST new account for client
Description: Creates a new account for an existing client.  URL specifies the client ID and and the account ID is auto generated.
             The amount field must be specified.  Returns an error if more than the amount field is given, the client ID is invalid, 
             or the amount provided is below zero.
*/
app.post("/clients/:id/accounts", async (req, res) =>
{
    const id:number = Number(req.params.id);
    try
    {
        if(Object.keys(req.body).length > 1)
        {
            res.status(403);
            res.send({"message": "Error. Cannot take in other properties other than amount."});
            return;
        }
        else
        {
            const bankClient:BankClient = await bankClientService.retrieveBankClientById(id);
        }
    }
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return; // We use return as to make sure we do not send multiple responses for our
        }
    }

    let bankAccount:BankAccount = req.body; // JS Object
    if (bankAccount.amount > 0)
    {
        bankAccount.clientId = id;
        bankAccount = await bankAccountService.registerBankAccount(bankAccount);
        res.status(201);
        res.send(bankAccount);
    }
    else
    {
        res.status(403);
        res.send({"message": "Error. Amount must be positive."})
    }
});

/*
Endpoint: GET accounts of a specific client
Description: Verifies that the client ID is valid, then makes a list with all bank accounts for every client and iterates through them
             choosing the ones that have a matching client_id with the URL id.  Returns an error message if the client does not have any
             accounts or the client ID is not valid.
*/
app.get("/clients/:id/accounts", async (req, res) =>
{
    const id:number = Number(req.params.id);
    try
    {
        const bankClient:BankClient = await bankClientService.retrieveBankClientById(id);
    }
    catch (error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
            return;
        }
    }

    const bankAccounts:BankAccount[] = await bankAccountService.retrieveAllBankAccounts();
    const bankClientAccounts:BankAccount[] = [];
    for (let i = 0; i < bankAccounts.length; i++)
    {
        if (bankAccounts[i].clientId === id)
        {
            bankClientAccounts.push(bankAccounts[i]);
        }
    }
    if (bankClientAccounts.length === 0) // If the client has no accounts, we return a success, but not an empty array
    {
        res.status(404);
        res.send({"message": `The client with client_id ${id} has no accounts.`});
        return;
    }
    res.status(200);
    res.send(bankClientAccounts);    
});

/*
Endpoint: GET account with specific ID
Description: Gets all accounts matching the specified URL ID.  Returns an error if the ID does not match
             any existing accounts.
*/
app.get("/accounts/:id", async (req, res) =>
{
    try
    {
        const bankAccountId = Number(req.params.id);
        const bankAccount:BankAccount = await bankAccountService.retrieveBankAccountById(bankAccountId);
        res.status(200);
        res.send(bankAccount);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

/*
Endpoint: PUT updates the account information (currently only amount)
Description: Must specify the account_id, the client_id, and the new amount of the account.  Returns an error if more or less fields
             are given, and if the ID of the URL does not match any existing account_id values.
*/
app.put("/accounts/:id", async (req, res) =>
{
    try
    {
        if(Object.keys(req.body).length > 3)
        {
            throw new MissingResourceError("message: Cannot add properties other than account_id, client_id, and amount.");
        }
        const id = Number(req.params.id);
        let bankAccount:BankAccount = req.body; // gives you all the info you need
        if (id === req.body.accountId)
        {
            bankAccount = await bankAccountService.modifyBankAccount(bankAccount);
            res.status(200);
            res.send(bankAccount);
        }
        else
        {
            res.status(403);
            res.send({"message": "Error. ID of URL must match body ID."});
        }
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404); 
            res.send(error);
        }
    }
});

/*
Endpoint: DELETE an account
Description: Deletes a account with a matching ID of the URL.  Returns an error if the account_id does not exist.
*/
app.delete("/accounts/:id", async (req, res) =>
{
    try
    {
        const bankAccountId = Number(req.params.id);
        const bankAccountExists = await bankAccountService.removeBankAccount(bankAccountId);
        res.status(205);
        res.send(bankAccountExists);
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

/*
Endpoint: PATCH updates the amount value (adds to it)
Description: Specifies an account and reads the account's current amount value, then adds to that with the given amount
             and updates the amount value in the account table.  Returns an error if the account ID does not exist.
*/
app.patch("/accounts/:id/deposit", async (req, res) =>
{
    try
    {
        const bankAccountId = Number(req.params.id);
        let bankAccount = await bankAccountService.retrieveBankAccountById(bankAccountId);
        bankAccount.amount += Number(req.body["amount"]); // bracket notation to access "amount" parameter of body
        bankAccount = await bankAccountService.modifyBankAccount(bankAccount);
        res.status(200);
        res.send(bankAccount); 
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});

/*
Endpoint: PATCH updates the amount value (subtracts from it)
Description: Specifies an accoutn and reads the account's current amount value, then subtracts from that with the given amount
             as long as the amount is less than or equal to the current account amount and updates the amount value in the 
             account table.  Returns an error if the account ID does not exist or the amount to withdraw from the account is 
             greater than the value of the amount within the account.
*/
app.patch("/accounts/:id/withdraw", async (req, res) =>
{
    try
    {
        const bankAccountId = Number(req.params.id);
        let bankAccount = await bankAccountService.retrieveBankAccountById(bankAccountId);
        if(Number(req.body["amount"]) > bankAccount.amount)
        {
            res.status(422);
            res.send({"message":"Cannot withdraw more than amount in account."});
            return;
        }
        bankAccount.amount -= Number(req.body["amount"]); // bracket notation to access "amount" parameter of body
        bankAccount = await bankAccountService.modifyBankAccount(bankAccount);
        res.status(200);
        res.send(bankAccount); 
    }
    catch(error)
    {
        if(error instanceof MissingResourceError)
        {
            res.status(404);
            res.send(error);
        }
    }
});


app.listen(3000, ()=>
{
    console.log("Application Started");
});