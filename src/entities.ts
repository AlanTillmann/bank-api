
export class BankClient
{
    constructor(public id:number, public fname:string){};
}

export class BankAccount
{
    constructor(
        public accountId:number,
        public clientId:number,
        public amount:number){};
}