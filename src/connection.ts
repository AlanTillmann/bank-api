import { Client } from "pg";

export const client = new Client( // used for creating a client object
{
    user:'postgres',
    password:process.env.DBPASSWORD, // you should NEVER STORE PASSWORDS IN CODE
    database:'bankingdb',
    port:5432,
    host:'35.193.90.118' //public IP address from GCP overview
})
client.connect();

// store passwords as environment variables then use: process.env.PASSWORD
// it is convention to capitalize the environment variable name
// use jest ./src/connection.spec.ts