import { client } from "../src/connection";
import { BankAccountDAO } from "../src/daos/bank-account-dao";
import { BankAccountDAOPostres } from "../src/daos/bank-account-dao-postgres-impl";
import { BankAccount, BankClient } from "../src/entities";

const bankAccountDAO:BankAccountDAO = new BankAccountDAOPostres();

// Any entity object that has not been saved somewhere should have the ID of 0
// This is a standard software convention
const testBankAccount:BankAccount = new BankAccount(1, 1, 1500);

test("Create an account", async ()=>
{
    const result:BankAccount = await bankAccountDAO.createBankAccount(testBankAccount);
    expect(result.accountId).not.toBe(0); // An entity that is saved should have a non-zero ID
});

test('Get all bank accounts', async ()=>
{
    let bankAccount1:BankAccount = new BankAccount(1, 1, 1000);
    let bankAccount2:BankAccount = new BankAccount(1, 1, 3200);
    let bankAccount3:BankAccount = new BankAccount(1, 2, 1700);
    await bankAccountDAO.createBankAccount(bankAccount1);
    await bankAccountDAO.createBankAccount(bankAccount2);
    await bankAccountDAO.createBankAccount(bankAccount3);

    const bankAccounts:BankAccount[] = await bankAccountDAO.getAllBankAccounts();

    expect(bankAccounts.length).toBeGreaterThanOrEqual(3);
});

test("Get bank account by Id", async ()=>
{
    let bankAccount:BankAccount = new BankAccount(1, 1, 1500);
    bankAccount = await bankAccountDAO.createBankAccount(bankAccount);
    let retrievedBankAccount:BankAccount = await bankAccountDAO.getBankAccountById(bankAccount.accountId);
    expect(retrievedBankAccount.accountId).toBe(bankAccount.accountId);
});

test('update account by Id', async ()=>
{
    let bankAccount:BankAccount = new BankAccount(1, 1, 1500);
    bankAccount = await bankAccountDAO.createBankAccount(bankAccount);

    // to update an object we just edit it and then pass it into a method
    bankAccount.amount = 2000;
    bankAccount = await bankAccountDAO.updateBankAccount(bankAccount);

    expect(bankAccount.amount).toBe(2000);
});

test('delete account by Id', async ()=>
{
    let bankAccount:BankAccount = new BankAccount(1, 1, 1500);
    bankAccount = await bankAccountDAO.createBankAccount(bankAccount);

    const result:boolean = await bankAccountDAO.deleteBankAccountById(bankAccount.accountId);
    expect(result).toBeTruthy();
});

afterAll(async()=>
{
    client.end(); // should close our connection when the test is over
});