import { client } from "../src/connection";
import { BankClientDAO } from "../src/daos/bank-client-dao";
import { BankClientDAOPostres } from "../src/daos/bank-client-dao-postgres-impl";
import { BankClient } from "../src/entities";

const bankClientDAO:BankClientDAO = new BankClientDAOPostres();

// Any entity object that has not been saved somewhere should have the ID of 0
// This is a standard software convention
const testBankClient:BankClient = new BankClient(0, 'Alan');

test("Create a client", async ()=>
{
    const result:BankClient = await bankClientDAO.createBankClient(testBankClient);
    expect(result.id).not.toBe(0); // An entity that is saved should have a non-zero ID
});

test('Get all bank clients', async ()=>
{
    let bankClient1:BankClient = new BankClient(0, 'Bob');
    let bankClient2:BankClient = new BankClient(0, 'Jimmy');
    let bankClient3:BankClient = new BankClient(0, 'Bill');
    await bankClientDAO.createBankClient(bankClient1);
    await bankClientDAO.createBankClient(bankClient2);
    await bankClientDAO.createBankClient(bankClient3);

    const bankClients:BankClient[] = await bankClientDAO.getAllBankClients();

    expect(bankClients.length).toBeGreaterThanOrEqual(3);
});

test("Get bank client by Id", async ()=>
{
    let bankClient:BankClient = new BankClient(0, 'Alan');
    bankClient = await bankClientDAO.createBankClient(bankClient);

    let retrievedBankClient:BankClient = await bankClientDAO.getBankClientById(bankClient.id);
    // difficult to maintain

    expect(retrievedBankClient.id).toBe(bankClient.id);
});

test('update client by Id', async ()=>
{
    let bankClient:BankClient = new BankClient(0, 'Bob');
    bankClient = await bankClientDAO.createBankClient(bankClient);

    // to update an object we just edit it and then pass it into a method
    bankClient.fname = 'Tom';
    bankClient = await bankClientDAO.updateBankClient(bankClient);

    expect(bankClient.fname).toBe('Tom');
});

test('delete client by id', async ()=>
{
    let bankClient:BankClient = new BankClient(0, 'Frank');
    bankClient = await bankClientDAO.createBankClient(bankClient);

    const result:boolean = await bankClientDAO.deleteBankClientById(bankClient.id);
    expect(result).toBeTruthy();
});

afterAll(async()=>
{
    client.end(); // should close our connection when the test is over
});
